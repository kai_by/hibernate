package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.entitys.CategoryEntity;
import ru.sber.jd.entitys.GoodEntity;
import ru.sber.jd.repositories.CategoryRepository;
import ru.sber.jd.repositories.GoodsRepository;

import java.util.Optional;
import java.util.Random;


@Service
@RequiredArgsConstructor
public class TestService implements CommandLineRunner {
    final GoodsRepository goodsRepository;
    final CategoryRepository categoryRepository;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Привет, Саня!!!");
        goodsRepository.saveAll(GoodEntity.getfirstdata());
        System.out.println(goodsRepository.count());
        System.out.println(String.format("Всего в таблице Goods %s записи: %s",goodsRepository.count(),goodsRepository.findAll()));
        goodsRepository.deleteById(1);
        System.out.println(String.format("Всего в таблице Goods %s записи: %s",goodsRepository.count(),goodsRepository.findAll()));
        categoryRepository.saveAll(CategoryEntity.getfirstdata());
        System.out.println(String.format("Всего  в таблице Category %s записи: %s",categoryRepository.count(),categoryRepository.findAll()));
        CategoryEntity newcat=new CategoryEntity();
        newcat.setName("Книги");
        newcat.setId(new Random().nextInt());
        categoryRepository.save(newcat);
        System.out.println(String.format("Всего  в таблице Category %s записи: %s",categoryRepository.count(),categoryRepository.findAll()));
        GoodEntity good=new GoodEntity();
        good.setCost(123);
        good.setId(new Random().nextInt());
        good.setCategoryId(newcat.getId());
        good.setName("Золушка");
        goodsRepository.save(good);
        System.out.println(String.format("Всего в таблице Goods %s записи: %s",goodsRepository.count(),goodsRepository.findAll()));
        System.out.println(String.format("Всего  в таблице Category %s записи: %s",categoryRepository.count(),categoryRepository.findAll()));
        System.out.println("Удаляем ошибочно введенный товар"+good);
        goodsRepository.delete(good);
        System.out.println(String.format("Всего в таблице Goods %s записи: %s",goodsRepository.count(),goodsRepository.findAll()));
        final Optional<GoodEntity> byId = goodsRepository.findById(4);
        if(byId.isPresent()) {
            good = byId.get();
            Optional<CategoryEntity> categoryRepositoryById = categoryRepository.findById(good.getCategoryId());
            if(categoryRepositoryById.isPresent())
                System.out.println(String.format("У товара %s категория %s",good.getName(),categoryRepositoryById.get().getName()));
        }
    }


}
