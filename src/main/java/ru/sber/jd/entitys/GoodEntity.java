package ru.sber.jd.entitys;

import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Getter
@Entity
@Data
public class GoodEntity {
    @Id
    @Column
    private Integer id;
    @Column
    private String name;
    @Column
    private float cost;
    @Column
    private int categoryId;

    public static List<GoodEntity> getfirstdata()
    {
        ArrayList<GoodEntity> goods=new ArrayList<>();
        GoodEntity goodEntity = new GoodEntity();
        goodEntity.setId(1);
        goodEntity.setName("Капуста");
        goodEntity.setCategoryId(41);
        goodEntity.setCost(20);
        goods.add(goodEntity);
        goodEntity=new GoodEntity();
        goodEntity.setId(2);
        goodEntity.setName("Плоскогубцы");
        goodEntity.setCategoryId(12);
        goodEntity.setCost(1300);
        goods.add(goodEntity);
        goodEntity=new GoodEntity();
        goodEntity.setId(3);
        goodEntity.setName("Радиоуправляемая машинка");
        goodEntity.setCategoryId(33);
        goodEntity.setCost(2200);
        goods.add(goodEntity);
        goodEntity=new GoodEntity();
        goodEntity.setId(4);
        goodEntity.setName("Тапочки");
        goodEntity.setCategoryId(24);
        goodEntity.setCost(200);
        goods.add(goodEntity);

//        "INSERT into GOODS values (1, 'Капуста', 41, 20);\n" +
//                "INSERT into GOODS values (2, 'Плоскогубцы', 12, 1300);\n" +
//                "INSERT into GOODS values (3, 'Радиоуправляемая машинка', 33, 2200);\n" +
//                "INSERT into GOODS values (4, 'Тапочки', 24, 200);";
        return goods;
    }

}
