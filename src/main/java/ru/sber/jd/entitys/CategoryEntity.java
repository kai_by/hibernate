package ru.sber.jd.entitys;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Data
@Entity
public class CategoryEntity {
    @Id
    @Column
    private Integer id;
    @Column
    private String name;

    static Map<Integer, String> data = Stream.of(new Object[][]{
            {41, "Овощи"},
            {12, "Инструменты"},
            {33,"Игрушки"},
            {24,"Обувь"},
    }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

    public static List<CategoryEntity> getfirstdata() {
        List<CategoryEntity> arr=new ArrayList<>();
        for(Map.Entry<Integer,String> entry:data.entrySet())
        {
            CategoryEntity cat=new CategoryEntity();
            cat.setId(entry.getKey());
            cat.setName(entry.getValue());
            arr.add(cat);
        }
        return arr;
    }
}
