package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.jd.entitys.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {
}

