package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entitys.GoodEntity;

@Repository
public interface GoodsRepository extends JpaRepository<GoodEntity, Integer> {
}

